This quick guide applies to 

* Poky: yocto-2.2.1 (morty-16.0.1)
* FSL Community BSP: 2.2
* QT: 5.7.0
* OS: Ubuntu 16.04 64-bit

# QT Application Development

## Installing QT Creator

Download and install QT Unified for Linux Online

```console
$: wget http://download.qt.io/official_releases/online_installers/qt-unified-linux-x64-online.run
$: chmod +x qt-unified-linux-x64-online.run
$: ./qt-unified-linux-x64-online.run
```

During installation, you can skip registration. Be sure to select Qt 5.7.0 and QtCreator (if not already selected)
Once the installation is complete, you need to add QtCreator bin folder to path

```console
$: export PATH=~/Qt/Tools/QtCreator/bin:$PATH
```


## Generating and installing the SDK

Refer to the section "Build the SDK" from [THIS PAGE](https://bitbucket.org/mobilesolutionsismb/rcs-bsp-platform/src/master/).
Once generated, you can install the SDK with the commands

```console
$: cd ~/ismb-pes-morty
$: ./build_fb/tmp/deploy/sdk/fslc-wayland-pes-glibc-x86_64-fsl-pes-qt5-armv7at2hf-neon-toolchain-2.2.1.sh
```

When prompted where to install the SDK, just press enter so to use the default directory

```console
FSLC Wayland PES SDK installer version 2.2.1
================================================
Enter target directory for SDK (default: /opt/fslc-wayland-pes/2.2.1): 
You are about to install the SDK to "/opt/fslc-wayland-pes/2.2.1". Proceed[Y/n]? y
 
Extracting SDK...........................................................................................................................done
Setting it up...done
SDK has been successfully set up and is ready to be used.
```

## Configuring QtCreator with the cross-toolchain
To configure QtCreator, the environment must be confgured according QT5 toolchain settings: 

```console
$: source /opt/fslc-wayland-pes/2.2.1/environment-setup-armv7at2hf-neon-fslc-linux-gnueabi
$: qtcreator
```

### 1. Device configuration
* Go to Tools => Options.
* In the left panel, click on Devices
* Add a new device representing the target "PES Device":
* Press Add and choose Generic Linux Device
* Specify a name (e.g. PES Device)
* Fill in the device's IP address.
* Please consider using fixed IP adresses: DHCP server lease timeout could lead to unreachable devices.
* Authentication on our modules by default: Password, User "root", empty password

### 2. Debugger configuration
* Go to Tools => Options
* In the left panel, click on Build&Run.
* Click on the Debuggers tab
* Press Add
* Specify a name (e.g. PES Device GDB)
* Specify the path: /opt/fslc-wayland-pes/2.2.1/sysroots/x86_64-fslcsdk-linux/usr/bin/arm-fslc-linux-gnueabi/arm-fslc-linux-gnueabi-gdb

### 3. GCC Compiler configuration
* Go to Tools => Options
* In the left panel, click on Build&Run.
* Click on the Compilers tab
* Press Add
* Specify a name (e.g. PES Device GCC)
* Specify the path: /opt/fslc-wayland-pes/2.2.1/sysroots/x86_64-fslcsdk-linux/usr/bin/arm-fslc-linux-gnueabi/arm-fslc-linux-gnueabi-gcc

### 4. G++ Compiler configuration
* Go to Tools => Options
* In the left panel, click on Build&Run.
* Click on the Compilers tab
* Press Add
* Specify a name (e.g. PES Device G++)
* Specify the path: /opt/fslc-wayland-pes/2.2.1/sysroots/x86_64-fslcsdk-linux/usr/bin/arm-fslc-linux-gnueabi/arm-fslc-linux-gnueabi-g++

### 5. QT version configuration
* Go to Tools => Options
* In the left panel, click on Build&Run.
* Click on the QT Version tab
* Press Add
* Specify a name (e.g. PES QT 5.x)
* Specify the path: /opt/fslc-wayland-pes/2.2.1/sysroots/x86_64-fslcsdk-linux/usr/bin/qt5/qmake

### 6. Kit configuration
* Go to Tools => Options
* In the left panel, click on Build & Run.
* Click on the Kits tab
* Press Add
* Name: PES Device
* Device: PES Device
* Sysroot: /opt/fslc-wayland-pes/2.2.1/sysroots/armv7at2hf-neon-fslc-linux-gnueabi
* Compiler: PES Device GCC
* Debugger: PES Device GDB
* Qt version: PES QT 5.x
* Qt mkspec: set to linux-oe-g++

## Application Deploying

When you run the application, Qt Creator copies the necessary files to the device and starts the application on it.

In your .pro file, remember to add the following lines to allow QT creator knowing where executable will be deployed:

```
target.path = /home/root
INSTALLS += target
```


