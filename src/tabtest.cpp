#include "tabtest.h"
#include "ui_tabtest.h"

#include <QDebug>
#include <QFuture>
#include <QtConcurrent/QtConcurrent>

#include <formulae.h>

#include "nfc_helper.h"
#include "nfc_device.h"
#include "GPIO/gpio_helper.h"

#define BOTTLE_DISTANCE 0.2f
#define ROTATION_ARM    0.08f
#define NUM_ARMS        4
#define MICROSTEPS      32

TabTest::TabTest(NFC_Helper *nfc, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TabTest)
{
    ui->setupUi(this);

    log = new Logger(ui->txtLog, ui->lbl_tagOk, ui->lbl_tagKo, ui->lbl_photocounter);

    this->nfc = nfc;
    this->nfc->AttachLogger(log);

    //========================== Stepper Motor GPIOS =============================
    GPIO *en = new GPIO(1, 5, GPIO::ActiveLevel::ACTIVE_LOW, "Enable");
    GPIO *dir = new GPIO(1, 4, GPIO::ActiveLevel::ACTIVE_HIGH, "Direction");

    PWM_Helper *stp = new PWM_Helper(1, 0, "Step");     // GPIO1_9
    stp->Init();

    motor = new Stepper(en, dir, stp, MICROSTEPS);

    //============================================================================

    //========================== Dial and up down buttons ========================
    connect(ui->dialBott, SIGNAL(valueChanged(int)), this, SLOT(onDialChanged()));
    connect(ui->btnLow,  &QPushButton::clicked, [=]() { ui->dialBott->setValue(ui->dialBott->value() -1 ); });
    connect(ui->btnHigh, &QPushButton::clicked, [=]() { ui->dialBott->setValue(ui->dialBott->value() +1 ); });

    // Set initial value
    ui->dialBott->setValue(ui->dialBott->minimum());

    //============================================================================

    //========================== ON/OFF Motor button =============================
    QState *off = new QState();
    off->assignProperty(ui->btnEnable, "text", "Off");
    off->setObjectName("off");
    off->assignProperty(ui->btnEnable, "styleSheet","background-color:rgb(190,190,190);");

    QState *on = new QState();
    on->setObjectName("on");
    on->assignProperty(ui->btnEnable, "text", "On");
    on->assignProperty(ui->btnEnable, "styleSheet","background-color:rgb(0,255,0);");

    off->addTransition(ui->btnEnable, SIGNAL(clicked()), on);
    on->addTransition(ui->btnEnable, SIGNAL(clicked()), off);

    machine.addState(off);
    machine.addState(on);

    machine.setInitialState(off);
    machine.start();

    connect(on, &QState::entered, [=]() {
        motor->EnableMotor(
            Formulae::CalculatePeriodStepFromBottlePassages(ui->dialBott->value() * 100, MICROSTEPS, NUM_ARMS)
        ); });

    connect(off, &QState::entered, [=]() {
        motor->DisableMotor();

    });

    //============================================================================

    connect(motor, &Stepper::MotorUpToSpeed, [=](){ ui->wdgControl->setEnabled(true); });

    connect(ui->btnRead, SIGNAL(clicked(bool)), this, SLOT(onBlockingRead()));
    connect(ui->btnPoll, SIGNAL(clicked(bool)), this, SLOT(onPolledRead()));
    connect(ui->btnStop, SIGNAL(clicked(bool)), nfc, SLOT(NFC_Abort()));

}

TabTest::~TabTest()
{
    delete ui;
}


void TabTest::updateUiInfo(int bottHour)
{
    float linear, freq, step_f, step_T, radius;
    this->device = nfc->selectedDevice;

    if(device != nullptr)
        ui->lblSelectedDevice->setText(device->name);

    if(bottHour == 0)
        return;

    ui->lblBotHour->setText(QString::number(bottHour));

    freq   = Formulae::CalculateFreqFromBottlesPassages(bottHour, NUM_ARMS);
    step_f = Formulae::CalculateFreqStepFromBottlePassages(bottHour, MICROSTEPS, NUM_ARMS);
    qDebug() << "Freq: "<< step_f;
    step_T = Formulae::CalculatePeriodStepFromBottlePassages(bottHour, MICROSTEPS, NUM_ARMS);
    ui->lblPeriodNs->setText(QString::number(step_T));

    linear = Formulae::CalculateLinearSpeedFromBottles(bottHour, BOTTLE_DISTANCE);
    ui->lblLinear->setText(QString::number(linear));

    radius = Formulae::CalculateExpectedRadius(linear, freq);
    ui->lblRadius->setText(QString::number(radius));

    /*
    angular = Formulae::CalculateAngularSpeedFromLinearSpeed(linear, ROTATION_ARM);
    ui->lblAngular->setText(QString::number(angular));

    rpm = Formulae::CalculateRPMFromAngularSpeed(angular, NUM_ARMS);
    ui->lblRpm->setText(QString::number(rpm));

    freq = Formulae::CalculateFreqFromAngularSpeed(angular, NUM_ARMS);


    ui->lblFreqStepHz->setText(QString::number(step_f));
    */


}


void TabTest::onDialChanged() {
    int bottles = ui->dialBott->value() * 100;

    if(motor->isOn()) {
        qDebug() << "Here!";
        ui->wdgControl->setEnabled(false);
        motor->Ramp2Period(Formulae::CalculatePeriodStepFromBottlePassages(bottles, MICROSTEPS, NUM_ARMS));
    }

    this->updateUiInfo(bottles);
}

void TabTest::onBlockingRead() {
    QFuture<void> future = QtConcurrent::run( [=]() {
        nfc->NFC_ReadUID(nfc->selectedDevice);
    });
}

void TabTest::onPolledRead() {
    QFuture<void> future = QtConcurrent::run( [=]() {
        nfc->NFC_PollUID(nfc->selectedDevice);
    });
}
