#-------------------------------------------------
#
# Project created by QtCreator 2018-06-07T16:39:10
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = NFC_Tester
TEMPLATE = app

LIBS += -lusb-1.0 -lnfc

# install
target.path = /home/root
INSTALLS += target

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SVN_BASE_DIR = $${PWD}/../
DESTDIR += $$SVN_BASE_DIR/build

DIR_NFC = $${PWD}/NFC/
include($$DIR_NFC/NFC.pri)

DIR_PWM = $${PWD}/PWM/
include($$DIR_PWM/PWM.pri)

DIR_GPIO = $${PWD}/GPIO/
include($$DIR_GPIO/GPIO.pri)

DIR_STEPPER = $${PWD}/STEPPER/
include($$DIR_STEPPER/STEPPER.pri)

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    tabdevices.cpp \
    tabtest.cpp \
    logger.cpp \
    formulae.cpp

HEADERS += \
    mainwindow.h \
    tabdevices.h \
    tabtest.h \
    logger.h \
    formulae.h

FORMS += \
    mainwindow.ui \
    tabdevices.ui \
    tabtest.ui

RESOURCES += \
    tabdevices.qrc
