#ifndef TABDEVICES_H
#define TABDEVICES_H

#include <QWidget>
#include <QListWidget>
#include "nfc_helper.h"

namespace Ui {
class TabDevices;
}

class TabDevices : public QWidget
{
    Q_OBJECT

public:
    explicit TabDevices(NFC_Helper *nfc, QWidget *parent = 0);
    ~TabDevices();



private:
    Ui::TabDevices *ui;
    NFC_Helper *nfc;

private slots:
    void ReadNFCDevicesList();
    void changeDeviceSelected(QListWidgetItem *current, QListWidgetItem *previous);

};

#endif // TABDEVICES_H
