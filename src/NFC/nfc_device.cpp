
#include "nfc_device.h"

NFC_Device::NFC_Device(QString connString, nfc_device *pnd, QObject *parent) : QObject(parent)
{
    this->connectionString = connString;

    this->name = nfc_device_get_name(pnd);

    char *strinfo = NULL;
    if(nfc_device_get_information_about(pnd, &strinfo) >= 0) {
        this->info = strinfo;
        nfc_free(strinfo);
    }
}

void NFC_Device::AttachLogger(Logger *log) {
    this->log = log;
}

bool NFC_Device::Open(nfc_context *context) {
    // OPEN DEVICE
    this->pnd = nfc_open(context, this->connectionString.toLatin1().data());
    return (pnd!=nullptr);
}

void NFC_Device::Close() {
    if(pnd!= nullptr)
        nfc_close(pnd);
}

bool NFC_Device::AbortCommand() {
    if(pnd!=nullptr)
        return nfc_abort_command(pnd);

    return false;
}
