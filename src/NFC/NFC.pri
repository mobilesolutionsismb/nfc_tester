SOURCES += \
    $${PWD}/nfc_helper.cpp \
    $$PWD/nfc_device.cpp

HEADERS += \
    $${PWD}/nfc_helper.h \
    $$PWD/nfc_device.h

INCLUDEPATH += \
    $${PWD}
