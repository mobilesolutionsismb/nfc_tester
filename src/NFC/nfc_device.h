#ifndef NFC_DEVICE_H
#define NFC_DEVICE_H

#include <QObject>
#include <QString>

#include "logger.h"
#include <nfc/nfc.h>

class NFC_Device : public QObject
{
    Q_OBJECT
public:
    explicit NFC_Device(QString connString, nfc_device *pnd, QObject *parent = nullptr);

    void AttachLogger(Logger *log);
    bool Open(nfc_context *context);
    void Close(void);

    bool AbortCommand(void);

signals:

public slots:

private:
    nfc_device *pnd;
    Logger *log;

public:
    QString     connectionString;
    QString     name;
    QString     info;
};
#endif // NFC_DEVICE_H
