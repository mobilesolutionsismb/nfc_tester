#include "nfc_helper.h"
#include "nfc/nfc.h"

#include <QDebug>
#include <inttypes.h>
#include <QDateTime>
#include <QLabel>
#include <QtMath>

#include "nfc_device.h"
#include "formulae.h"

static bool abort_test = true;

NFC_Helper::NFC_Helper(QObject *parent) : QObject(parent)
{

}

static void
print_nfc_target(const nfc_target *pnt, bool verbose)
{
  char *s;
  str_nfc_target(&s, pnt, verbose);
  qDebug(s);
  nfc_free(s);
}

bool NFC_Helper::NFC_Init() {
    nfc_init(&context);
    if (context == NULL) {
        return false;
    }

    libNfcVersion = nfc_version();

    return true;
}

void NFC_Helper::AttachLogger(Logger *log) {
    this->log = log;
}

int NFC_Helper::NFC_ListDevices() {

    this->list.clear();
    nfc_connstring connstrings[MAX_DEVICE_COUNT];
    size_t szDeviceFound = nfc_list_devices(context, connstrings, MAX_DEVICE_COUNT);

    for(size_t i = 0; i<szDeviceFound; i++) {
        nfc_device *pnd = nfc_open(context, connstrings[i]);
        if(pnd!=nullptr) {
            NFC_Device *device = new NFC_Device(connstrings[i], pnd);
            device->AttachLogger(log);
            this->list.insert(i, device);
            nfc_close(pnd);
        }
    }
    return szDeviceFound;
}


int NFC_Helper::NFC_ReadUID(NFC_Device *dev) {
    tagOk = 0;
    tagKo = 0;
    photocounter = 0;
    mismatchCounter = 0;
    uint8_t last_id = 0;
    abort_test = false;

    log->printTagOk(0);
    log->printTagKo(0);
    log->printPhotocell(0);


    if(dev == nullptr)
        dev = this->selectedDevice;

    if(dev == nullptr) {
        log->Info("No NFC device selected!");
        return -1;
    }

    const nfc_modulation nmModulations = { .nmt = NMT_ISO14443A, .nbr = NBR_106 };

    // OPEN DEVICE
    log->Info_noNL("Opening device...");
    log->Info(dev->connectionString.toLatin1().data());
    nfc_device *pnd = nfc_open(context, dev->connectionString.toLatin1().data());
    log->Info_Result(pnd!=nullptr);
    if(pnd==nullptr) return -1;

    this->pnd = pnd;

    // INITIATOR INIT
    log->Info_noNL("Initiator init...");
    int res = nfc_initiator_init(pnd);
    log->Info_Result(res>=0);
    if(res<0) {nfc_close(pnd); return -1;}

    nfc_target nt;
    QString out;

    QDateTime start(QDateTime::currentDateTime());
    qDebug() << "Starting test @ " << start;

    while(!abort_test) {
        res = nfc_initiator_select_passive_target(pnd, nmModulations, NULL, 0, &nt);


        if(res > 0 && nt.nti.nai.abtUid[1] != last_id) {
            tagOk++;
            log->printTagOk(tagOk);
            qDebug() << tagOk << " " << QString("0x%1").arg(nt.nti.nai.abtUid[1], 2, 16, QLatin1Char( '0' ));

        } else {
            tagKo++;
            log->printTagKo(tagKo);
            qDebug() << "Timeout or tag skipped " << QString("0x%1").arg(nt.nti.nai.abtUid[1], 2, 16, QLatin1Char( '0' ));
        }
        while(0 == nfc_initiator_target_is_present(pnd, NULL)) {}
        last_id = nt.nti.nai.abtUid[1];

    }
    QDateTime stop(QDateTime::currentDateTime());
    qDebug() << "Stopping test @ " << stop;

    qDebug() << "Results:";
    qDebug() << "  test duration   = " << Formulae::secondsToString(start.secsTo(stop));
    qDebug() << "  total bottles   = " << tagOk + tagKo;
    qDebug() << "     with success = " << tagOk;
    qDebug() << "     with error   = " << tagKo;

    nfc_close(pnd);
    return 0;
}

int NFC_Helper::NFC_PollUID(NFC_Device *dev) {
    int counter = 0, errors =0;
    uint8_t last_id = 0;
    abort_test = false;

    if(dev == nullptr)
        dev = this->selectedDevice;

    if(dev == nullptr) {
        log->Info("No NFC device selected!");
        return -1;
    }

    const uint8_t uiPollNr = 20;
    const uint8_t uiPeriod = 2;
    const nfc_modulation nmModulations[] = {
        { .nmt = NMT_ISO14443A, .nbr = NBR_106 },
        { .nmt = NMT_ISO14443B, .nbr = NBR_106 },
    };
    const size_t szModulations = 2;

    // OPEN DEVICE
    log->Info_noNL("Opening device...");
    log->Info(dev->connectionString.toLatin1().data());
    nfc_device *pnd = nfc_open(context, dev->connectionString.toLatin1().data());
    log->Info_Result(pnd!=nullptr);
    if(pnd==nullptr) return -1;

    this->pnd = pnd;

    // INITIATOR INIT
    log->Info_noNL("Initiator init...");
    int res = nfc_initiator_init(pnd);
    log->Info_Result(res>=0);
    if(res<0) {nfc_close(pnd); return -1;}

    nfc_target nt;

    while(!abort_test) {
        res = nfc_initiator_poll_target(pnd, nmModulations, szModulations, uiPollNr, uiPeriod, &nt);
        if(res > 0 && nt.nti.nai.abtUid[1] != last_id) {
            counter++;
            qDebug() << counter << " " << QString("0x%1").arg(nt.nti.nai.abtUid[1], 2, 16, QLatin1Char( '0' ));
        } else {
            errors++;
            qDebug() << "Timeout or tag skipped " << QString("0x%1").arg(nt.nti.nai.abtUid[1], 2, 16, QLatin1Char( '0' ));
        }
        last_id = nt.nti.nai.abtUid[1];
    }
    nfc_close(pnd);

    return 0;

}

void NFC_Helper::NFC_Abort() {
    abort_test = true;    
}

void NFC_Helper::UpdatePhotocellCounter(bool value) {
    //qDebug() << "*";
    if(value == true && !abort_test) {
        photocounter++;
        log->printPhotocell(photocounter);
        if(qFabs(photocounter - tagOk)!= mismatchCounter) {
            mismatchCounter = qFabs(photocounter - tagOk) ;
            qDebug() << "Mismatch counter= " << mismatchCounter;
        }
    }
}

