#ifndef NFC_HELPER_H
#define NFC_HELPER_H

#include <QObject>
#include <QMap>
#include <QLabel>

#include <nfc/nfc.h>
#include "logger.h"

#include "nfc_device.h"

#define MAX_DEVICE_COUNT    16
#define MAX_TARGET_COUNT    16

class NFC_Helper : public QObject
{
    Q_OBJECT
public:
    explicit NFC_Helper(QObject *parent = nullptr);
    bool    NFC_Init();
    int     NFC_ListDevices();
    void    AttachLogger(Logger *log);

    int     NFC_ReadUID(NFC_Device *dev);
    int     NFC_PollUID(NFC_Device *dev);

    NFC_Device *selectedDevice;
    nfc_device *pnd;

signals:

public slots:
    void NFC_Abort(void);
    void UpdatePhotocellCounter(bool value);

public:
    QString                     libNfcVersion;
    QMap<int, NFC_Device *>     list;

private:
    Logger *log;
    nfc_context *context;
    int tagOk, tagKo, photocounter, mismatchCounter;
};

#endif // NFC_HELPER_H
