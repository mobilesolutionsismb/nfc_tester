#ifndef TABTEST_H
#define TABTEST_H

#include <QWidget>
#include <QStateMachine>
#include "logger.h"
#include "NFC/nfc_helper.h"
#include "GPIO/gpio_helper.h"
#include "PWM/pwm_helper.h"
#include "STEPPER/stepper.h"

namespace Ui {
class TabTest;
}

class TabTest : public QWidget
{
    Q_OBJECT

public:
    explicit TabTest(NFC_Helper *nfc, QWidget *parent = 0);
    ~TabTest();

    void updateUiInfo(int bottHour);

private:
    Ui::TabTest *ui;
    NFC_Helper *nfc;
    NFC_Device *device;
    Stepper *motor;

    QStateMachine machine;

    Logger *log;

public slots:

    void onDialChanged(void);

    void onBlockingRead(void);
    void onPolledRead(void);
};

#endif // TABTEST_H
