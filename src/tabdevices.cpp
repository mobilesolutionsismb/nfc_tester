#include "tabdevices.h"
#include "ui_tabdevices.h"
#include <QDebug>
#include <QSignalMapper>

#include "nfc_helper.h"



#define MAX_DEVICE_COUNT 16
#define MAX_TARGET_COUNT 16


TabDevices::TabDevices(NFC_Helper *nfc, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TabDevices)
{
    this->nfc = nfc;
    nfc->selectedDevice = nullptr;
    ui->setupUi(this);


    ui->lblVersion->setText(nfc->libNfcVersion);

    this->ReadNFCDevicesList();

    ui->listWidget->setViewMode(QListView::ListMode);
    ui->listWidget->setIconSize(QSize(32, 32));
    ui->listWidget->setMovement(QListView::Static);
    ui->listWidget->setMaximumWidth(400);
    ui->listWidget->setSpacing(2);

    connect(ui->btnScan, SIGNAL(clicked(bool)), this, SLOT(ReadNFCDevicesList()));

    connect(ui->listWidget, &QListWidget::currentItemChanged, this, &TabDevices::changeDeviceSelected);
}

TabDevices::~TabDevices()
{
    delete ui;
}

void TabDevices::ReadNFCDevicesList()
{
    int devNum = nfc->NFC_ListDevices();
    QMapIterator<int, NFC_Device*> i(nfc->list);

    ui->listWidget->clear();
    ui->lblName->clear();
    ui->lblConnString->clear();
    ui->lblInfo->clear();

    while(i.hasNext()) {
        i.next();
        NFC_Device *dev = i.value();
        QListWidgetItem *item = new QListWidgetItem(ui->listWidget);
        if(dev->connectionString.contains("usb"))
            item->setIcon(QIcon(":/images/usb.png"));
        else if(dev->connectionString.contains("uart"))
            item->setIcon(QIcon(":/images/serial.png"));
        else
            item->setIcon(QIcon(":/images/unknown.png"));

        item->setText(dev->name);
        item->setTextAlignment(Qt::AlignVCenter);
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        item->setData(Qt::UserRole, i.key());
    }

    return;
}

void TabDevices::changeDeviceSelected(QListWidgetItem *current, QListWidgetItem *previous)
{
    if (!current)
        current = previous;

    int key = current->data(Qt::UserRole).toUInt();
    NFC_Device *dev = nfc->list.value(key);

    nfc->selectedDevice = dev;

    ui->lblName->setText(dev->name);
    ui->lblConnString->setText(dev->connectionString);
    ui->lblInfo->setText(dev->info);

}
