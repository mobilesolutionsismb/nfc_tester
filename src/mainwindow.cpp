#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "tabdevices.h"
#include "tabtest.h"
#include <QMessageBox>
#include <QDebug>
#include <libusb-1.0/libusb.h>

#include "nfc_helper.h"
#include "GPIO/gpio_monitor.h"




MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    NFC_Helper *nfc = new NFC_Helper();
    nfc->NFC_Init();

    ui->tabWidget->addTab(new TabDevices(nfc),"Devices");
    ui->tabWidget->addTab(new TabTest(nfc), "Test" );

    connect(ui->tabWidget, SIGNAL(currentChanged(int)), this, SLOT(onTabChanged(int)));

    qRegisterMetaType<QTextBlock>("QTextBlock");
    qRegisterMetaType<QTextCursor>("QTextCursor");

    //GPIOMonitor *monitor1 = new GPIOMonitor(4,22, "Photocell_1");
    //monitor1->Enable();
    GPIOMonitor *monitor2 = new GPIOMonitor(4,23, "Photocell_2");
    monitor2->Enable();

    connect(monitor2, SIGNAL(valueChanged(bool)), nfc, SLOT(UpdatePhotocellCounter(bool)));


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onTabChanged(int tabIdx) {
    if(ui->tabWidget->tabText(tabIdx) == "Test") {
        TabTest *tabTest = (TabTest *)ui->tabWidget->currentWidget();
        tabTest->updateUiInfo(0);
    }

}
