#ifndef FORMULAE_H
#define FORMULAE_H

#include <QtMath>
#include <QString>
#include <QDateTime>
#include <QTime>

class Formulae
{
public:
    // Calculations helpers

    static float CalculateBottlesPerSecond(int nBot);
    static float CalculateFreqFromBottlesPassages(int nBot, int arms);
    static long  CalculateFreqStepFromBottlePassages(int nBot, int microsteps, int arms);
    static long  CalculatePeriodStepFromBottlePassages(int nBot, int microsteps, int arms);

    static float CalculateLinearSpeedFromBottles(int nBot, float dBot);

    static float CalculateExpectedRadius(float linSpeed, float freq);

    static QString secondsToString(qint64 seconds);
    //static long  CalculatePeriodStepFromBottles(int nBot, float dBot, float radius, int arms, int microsteps);



};

#endif // FORMULAE_H
