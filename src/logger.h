#ifndef LOGGER_H
#define LOGGER_H

#include <QObject>
#include <QTextEdit>
#include <QTextBlock>
#include <QLabel>

Q_DECLARE_METATYPE(QTextBlock)
Q_DECLARE_METATYPE(QTextCursor)

class Logger : public QObject
{
    Q_OBJECT

public:
    explicit Logger(QTextEdit *txtLog, QLabel *tagOk, QLabel *tagKo, QLabel *photocounter, QObject *parent = nullptr);

    void Info(QString s);
    void Info_noNL(QString s);
    void Info_Result(bool result);
    void Info_Hex(const uint8_t *pbtData, const size_t szBytes, bool newLine=true);

    void printTagOk(int numOk);
    void printTagKo(int numKo);
    void printPhotocell(int numPhotocell);
signals:

public slots:

private:
    QTextEdit *logWidget;
    QLabel *tagOk, *tagKo, *photocounter;
    void scrollToCaret();


};

#endif // LOGGER_H
