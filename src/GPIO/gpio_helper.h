#ifndef GPIO_HELPER_H
#define GPIO_HELPER_H

#include <QObject>
#include <QString>
#include <QFile>

class GPIO : public QObject
{
    Q_OBJECT
public:
    enum ActiveLevel {ACTIVE_LOW, ACTIVE_HIGH,UNKNOWNLEVEL};
    Q_ENUM(ActiveLevel)

    explicit GPIO(int bank, int pin, ActiveLevel level, QString name, QObject *parent = nullptr);

    bool Open();
    bool SetValue(int val);
    bool On();
    bool Off();

signals:
public slots:

private:
    QString SysFsValue;
    QFile *SysFsFile;

    int bank;
    int pin;
    ActiveLevel activeState;
    QString name;
};

#endif // GPIO_HELPER_H
