#include "gpio_helper.h"
#include <QDebug>
#include <QFile>

GPIO::GPIO(int bank, int pin, ActiveLevel level, QString name, QObject *parent):QObject(parent)
{


    this->pin = pin;
    this->bank = bank>=1?bank:1;
    this->activeState = level;
    this->name = name;

    this->SysFsValue = QString("/sys/class/gpio/gpio%1/value").arg(((bank -1 ) * 32)+pin);
    this->SysFsFile = NULL;

    qDebug() << this->SysFsValue;

}

bool GPIO::Open() {
    QFile *valueFile = new QFile(SysFsValue);
    if (!valueFile->open(QIODevice::WriteOnly | QIODevice::Text)) {
        qDebug() << "error in open gpio";
        return false;
    }

    this->SysFsFile = valueFile;
    return true;
}

bool GPIO::SetValue(int val) {
    bool e = this->Open();
    if(e == true) {
        QTextStream out(this->SysFsFile);
        out << QString::number(val);
        qDebug() << "Set value ok: " << val << " in " << this->SysFsValue;
        SysFsFile->close();

        return true;
    }
    qDebug() << "error in setvalue gpio";
    return e;
}

bool GPIO::On()
{
    return this->SetValue(this->activeState==ACTIVE_HIGH?1:0);
}

bool GPIO::Off()
{
   return this->SetValue(this->activeState == ACTIVE_HIGH?0:1);
}
