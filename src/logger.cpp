#include "logger.h"
#include <QTextEdit>
#include <QLabel>

Logger::Logger(QTextEdit *txtLog, QLabel *tagOk, QLabel *tagKo, QLabel *photocounter, QObject *parent) : QObject(parent)
{
    this->logWidget = txtLog;
    this->tagOk = tagOk;
    this->tagKo = tagKo;
    this->photocounter = photocounter;
}

void Logger::Info(QString s) {
    logWidget->insertPlainText(s+"\n");
    scrollToCaret();
}


void Logger::Info_noNL(QString s) {
    logWidget->insertPlainText(s);
    //scrollToCaret();
}

void Logger::Info_Result(bool result) {
    QString s = QString("[%1]\n").arg(result?"OK":"ERROR");
    Info(s);
}

void Logger::Info_Hex(const uint8_t *pbtData, const size_t szBytes, bool newLine)
{
    size_t  szPos;
    QString out;

    for (szPos = 0; szPos < szBytes; szPos++) {
        QString tmp;
        tmp.sprintf("%02x  ", pbtData[szPos]);
        out.append(tmp);
    }
    if(newLine)
        out.append("\r\n");

    Info_noNL(out);
}

void Logger::printTagOk(int numOk) {
    tagOk->setText(QString::number(numOk));
}

void Logger::printTagKo(int numKo){
    tagKo->setText(QString::number(numKo));
}

void Logger::printPhotocell(int numPhotocell) {
    photocounter->setText(QString::number(numPhotocell));
}

void Logger::scrollToCaret() {
    QTextCursor c = logWidget->textCursor();
    c.movePosition(QTextCursor::End);
    logWidget->setTextCursor(c);
}
